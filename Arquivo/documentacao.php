<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Home | Projeto PT-br - O corpus do português não padrão</title>
<?php
// inicia a sessão
session_start ();
// se a sessão for falsa, volta pro login
if (isset ( $_SESSION ["Logado"] ) == false) {
	include './menu.php';
}else{
	include("./menuLogado.php");
}

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon"
	href="http://icon-icons.com/icons2/317/PNG/512/book-bookmark-icon_34486.png">

</head>

<body class="Homepage no-sidebar" id="home">
	
	<br>
	<br>
	<!-- icones -->
	<div class="container">
		<div class="row text-center homepage">
			<div class="col-sm-12">
				<div class="form-label">
					<h3 class="text-success text-center">Documentação</h3>
				</div>
				<br>
				<div class="form-label">
					<p>Desculpe-nos caro usuário, mas ainda não concluímos o
						desenvolvimento desta página. Ela estará plenamente finalizada ao
						fim deste projeto de pesquisa (início de 2018/2) e estará
						disponível para uso, ilimitavelmente.</p>
				</div>
				<br>
				<div class="form-label">
					<img style="width: 45%;" alt="Alerta Página em Construção"
						src="./_css/_img/icon-obra.png">
				</div>
			</div>
		</div>
	</div>
	<?php include 'rodape.php';?>


</body>
</html>
