<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Quem Somos | Projeto PT-br</title>
<?php
// inicia a sessão
session_start ();
// se a sessão for falsa, volta pro login
if (isset ( $_SESSION ["Logado"] ) == false) {
	include ("./menu.php");
} else {
	include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">
<link rel="stylesheet" type="text/css" href="./_css/nos.css">

</head>
<body>
	<br>
	<div class="container-left">
		<div class="row text-left">
			<div class="col-sm-12">
				<div class="form-label">
					<h3>Atuais Membros da Equipe:</h3>
				</div>
				<br>
				<table>
					<thead>
						<tr  style="border-top: 1px solid gray;">
							<td><p>Foto</p></td>
							<td><p>Nome</p></td>
							<td><p>Período</p></td>
							<td><p>Tipo</p></td>
							<td><p>MicroBiografia</p></td>
							<td><p>Lattes</p></td>
						</tr>
					</thead>
					<tbody>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Profº Me. Andrei Inácio"
								src="./_css/_img/andrei.png"></td>
							<td><p>Andrei de Souza Inácio</p></td>
							<td><p>2016-Atual</p></td>
							<td><p>Coordenador</p></td>
							<td><p>Possui graduação em Ciências da Computação pela
									Universidade Federal de Santa Catarina (2013) e mestrado em
									Ciência da Computação da Universidade Federal de Santa Catarina
									- UFSC (2016). Atualmente é professor de informática do
									Instituto Federal de Santa Catarina(IFSC) e pesquisador do
									Instituto Nacional de Ciência e Tecnologia para Convergência
									Digital (INCoD). Faz parte do grupo de Pesquisa em Tecnologias
									da Informação e Comunicação (IFSC) e coordena este projeto
									desde 2017.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/1403044049218262">http://lattes.cnpq.br/1403044049218262</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Profª Dra. Caroline Reis"
								src="./_css/_img/doutora.png"></td>
							<td><p>Profª Dra. Caroline Reis Vieira Santos Rauta</p></td>
							<td><p>2016-Atual</p></td>

							<td><p>Pesquisadora</p></td>
							<td><p>É graduada em Língua e Literatura Vernáculas, mestra e
									doutora em Estudos da Tradução pela Iniversidade Federal de
									Santa Catarina. Desde 2014 é professora do Instituto Federal de
									Santa Catarina, campûs Gaspar, onde leciona para cursos
									técnicos, superiores e pós-graduações. Faz parte do grupo de
									Pesquisa em Tecnologias da Informação e Comunicação (IFSC) e
									coordenou este projeto entre 2016 e 2017.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnqp.br/3519380803739395" target="_blank">http://lattes.cnqp.br/3519380803739395</a>
								</p></td>

						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Bolsista Paulo Henrique dos Santos"
								src="./_css/_img/pauloComBordinha.png"></td>
							<td><p>Paulo Henrique dos Santos</p></td>
							<td><p>2018-Atual</p></td>
							<td><p>Bolsista</p></td>
							<td><p>Cursou o ensino fundamental na escola de ensino básico Professor Vitorio Anacleto Cardoso (2015). Cursa o ensino médio integrado ao curso técnico em informática no IFSC- Campus Gaspar. Fez parte, como voluntario na bolsa para a Olimpíada Brasileira de Informática (2016), fez parte do grupo de robótica DinoSaulos (2017).</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/9591067516244724">http://lattes.cnpq.br/9591067516244724</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Bolsista Lucas Lima da Cunha"
								src="./_css/_img/LucasComBordinha.png"></td>
							<td><p>Lucas Lima da Cunha</p></td>
							<td><p>2018-Atual</p></td>
							<td><p>Bolsista</p></td>
							<td><p>

Cursou o ensino fundamental na escola de ensino básico Marcos Konder (2015), e atualmente cursa o ensino médio integrado ao curso técnico em informática no IFSC - Campus Gaspar. Fez parte, como bolsista do projeto Arte Urbana (2016-2017) e foi também membro ativo do grêmio estudantil (2011-2014 e 2017).
</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/4929320434241041">http://lattes.cnpq.br/4929320434241041</a>
								</p></td>
						</tr>

						
					</tbody>
				</table>

				<br> <br>
				<div class="form-label">
					<h3>Antigos Membros da Equipe:</h3>
				</div>
				<br>
				<table>
					<thead>
						<tr  style="border-top: 1px solid gray;">
							<td><p>Foto</p></td>
							<td><p>Nome</p></td>
							<td><p>Período</p></td>
							<td><p>Tipo</p></td>
							<td><p>MicroBiografia</p></td>
							<td><p>Lattes</p></td>
						</tr>
					</thead>
					<tbody>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Profº Dr. Delcino Picini Jr"
								src="./_css/_img/delcino.png"></td>
							<td><p>Prof. Dr. Delcino Picini Jr</p></td>
							<td><p>2016-2016</p></td>
							<td><p>Pesquisador</p></td>
							<td><p>Doutorado em Educação (Psicologia da Educação) pela
									Pontifícia Universidade Católica de São Paulo, SP (2003).
									Mestrado em Educação Matemática pela Pontifícia Católica de São
									Paulo, SP (1991). Graduação em Pedagogia pela Faculdade de
									Filosofia Ciências e Letras de São Bernardo do Campo, SP
									(1979). Graduação em Matemática pela Faculdade de Filosofia
									Ciências e Letras de São Caetano do Sul, SP (1975). Desde
									08/2006 é professor Pleno do Centro Paula Souza, na Faculdade
									de Tecnologia de São Bernardo do Campo, SP, lecionado na área
									de Matemática. Fez parte do grupo de Pesquisa em Tecnologias da
									Informação e Comunicação (IFSC) no ano de 2016.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/4621284276761338">http://lattes.cnpq.br/4621284276761338</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Profº Me. Alexandre Altair de Melo"
								src="./_css/_img/alexandre.png"></td>
							<td><p>Prof. Me. Alexandre Altair de Melo</p></td>
							<td><p>2016-2016</p></td>
							<td><p>Pesquisador</p></td>
							<td><p>Possui graduação em Sistemas de Informação pela
									Universidade da Região de Joinville (UNIVILLE-2005),
									Pós-Graduação em Planejamento e Gerenciamento Estratégico pela
									Pontifícia Universidade Católica do Paraná (PUCPR-2007) e
									Mestrado em Computação Aplicada pela Universidade do Estado de
									Santa Catarina (UDESC-2016). Atualmente é professor de
									informática pelo Instituto Federal de Santa Catarina (IFSC).
									Fez parte do grupo de Pesquisa em Tecnologias da Informação e
									Comunicação (IFSC) no ano de 2016.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/2886482777094653">http://lattes.cnpq.br/2886482777094653</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid black;">
							<td class="text-center"><img width="70%"
								alt="Foto Profº Me. Leonardo Ronald Perin Rauta"
								src="./_css/_img/rauta.png"></td>
							<td><p>Prof. Me. Leonardo Ronald Perin Rauta</p></td>
							<td><p>2016-2016</p></td>
							<td><p>Pesquisador</p></td>
							<td><p>Leonardo Rauta é Mestre em Computação Aplicada e graduado
									em Engenharia de Computação pela Universidade do Vale do Itajaí
									- UNIVALI. Atualmente é professor no Instituto Federal de Santa
									Catarina - Campûs Gaspar, onde leciona na área de informática.
									Fez parte do grupo de Pesquisa em Tecnologias da Informação e
									Comunicação (IFSC) no ano de 2016.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/6399151468502223">http://lattes.cnpq.br/6399151468502223</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Ex-Bolsista Lucas Schmidt"
								src="./_css/_img/schmidt.png"></td>
							<td><p>Lucas Schimidt</p></td>
							<td><p>2016-2016</p></td>
							<td><p>Ex-Bolsista</p></td>
							<td><p>Possui 1º Grau pela Escola Adventista de Blumenau (2014).
									Tem experiência na área de Robótica, Mecatrônica e Automação,
									com ênfase em Tecnico em Informatica. Fez parte do grupo de
									Pesquisa em Tecnologias da Informação e Comunicação (IFSC) no
									ano de 2016.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/5129723211386846">http://lattes.cnpq.br/5129723211386846</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Bolsista Matheus Filipi"
								src="./_css/_img/matheus.png"></td>
							<td><p>Matheus Filipi Pereira da Silva</p></td>
							<td><p>2016-2018</p></td>
							<td><p>Bolsista</p></td>
							<td><p>Tem experiência na área de Robótica, Mecatrônica e
									Automação, com ênfase em Robótica, Mecatrônica e Automação. Faz
									parte do grupo de Pesquisa em Tecnologias da Informação e
									Comunicação (IFSC).</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/3360878279276330">http://lattes.cnpq.br/3360878279276330</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center"><img width="70%"
								alt="Foto Bolsista Ricardo Vargas"
								src="./_css/_img/ricardo.png"></td>
							<td><p>Ricardo Silva Vargas</p></td>
							<td><p>2016-2018</p></td>
							<td><p>Bolsista</p></td>
							<td><p>Não possui formação. Desde 2015 é estudante no Instituto
									Federal de Santa Catarina - Campûs Gaspar, onde cursa o curso
									Técnico em Informática Integrado ao Ensino Médio. Os projetos
									de pesquisa nos quais participa ou participou, envolvem as
									áreas de robótica, programação, banco de dados, programação
									para jogos, história, linguística, entre outras. Faz parte do
									grupo de Pesquisa em Tecnologias da Informação e Comunicação
									(IFSC), mas deseja envolver-se em outros grupos de pesquisa,
									trabalhando nas mais diversas áreas.</p></td>
							<td><p>
									Link para o Lattes: <a
										href="http://lattes.cnpq.br/4062288358660062">http://lattes.cnpq.br/4062288358660062</a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center">Foto</td>
							<td><p>Larissa de Gasperi Firmes</p></td>
							<td><p>2016-2016</p></td>
							<td><p>Ex-Bolsista</p></td>
							<td><p>MicroBiografia</p></td>
							<td><p>
									Link para o Lattes: <a href=""></a>
								</p></td>
						</tr>
						<tr style="border-top: 1px solid gray;">
							<td class="text-center">Foto</td>
							<td><p>Fernanda Mateussi Garcia de Souza</p></td>
							<td><p>2016-2016</p></td>
							<td><p>Ex-Bolsista</p></td>
							<td><p>MicroBiografia</td>
							<td><p>Link para o Lattes:</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
</body>
</html>
<?php

include 'rodape.php';
