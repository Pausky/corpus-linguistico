<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Menu Logado</title>
</head>
<body>
	<header>
		<br></br> <br></br> <br></br>
		<p id="logo">Corpus Linguístico</p>
		<br></br>
	</header>

	<!-- menu horizontal -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand img-responsive" href="./index.php"> <img
					src="./_css/_img/Logo.png" width="80%" alt="logo">
				</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="link dropdown-toggle" data-toggle="dropdown">Descubra<b
							class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="./projeto.php" class="link">O que é o Projeto PT-br</a></li>
							<!-- <li><a href="./captura.php" class="link">Capturas de tela</a></li>-->
						</ul></li>
					<li><a class="link dropdown-toggle" data-toggle="dropdown">Acesse
							Já<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
							<li><a href="./buscar.php" class="link">Busca ao Corpus</a></li>
							<li><a href="./aovivo.php" class="link">Busca ao Vivo</a></li>
							<!-- <li><a href="./recursos.php" class="link">Recursos</a></li>-->
							<li><a href="./documentacao.php" class="link">Documentação</a></li>
						</ul></li>
					<li class=""><a class="link dropdown-toggle" data-toggle="dropdown">Ajuda<b
							class="caret"></b></a>
						<ul class="dropdown-menu">
							<!--<li><a href="./discussao.php" class="link">Listas de discussão</a></li>-->
							<!--<li><a href="./forum.php" class="link">Fórum</a></li>-->
							<!--<li><a href="./suporte.php" class="link">Suporte</a></li>-->
							<li><a href="./bugs.php" class="link">Assistente de bugs</a></li>
						</ul></li>
					<li><a class="link dropdown-toggle" data-toggle="dropdown">Sobre
							nós<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
							<li><a href="./nos.php" class="link">Quem somos</a></li>
							<!--<li><a href="./creditos.php" class="link">Créditos</a></li>-->
							<li><a href="./contato.php" class="link">Contato</a></li>
						</ul></li>
					<li><a class="link dropdown-toggle" data-toggle="dropdown"><?php echo "" . $_SESSION['nome'] ." " .$_SESSION['sobrenome'] ?>
					<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<!-- <li><a href="perfil.php"
								class="link">Perfil</a></li>-->
							<!-- <li><a
								href="?<?php echo "" .$_SESSION['username'] ."/account" ?>"
								class="link" onclick="perfil.php">Configurações de Conta</a></li>-->
							<li><a href="./deslogar.php" class="link">Sair</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>