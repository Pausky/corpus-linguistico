<?php
// requer os arquivos de conexao ao DB e para enviar email
require_once ('./configurar.php');
require ("./APIs/phpmailer/class.phpmailer.php");
require ('./APIs/phpmailer/PHPMailerAutoload.php');
?>

<html>
<head>
<title>Redefinição de Senha | Corpus Linguístico</title>
<?php
session_start ();

if (isset ( $_SESSION ["Logado"] ) == false) {
	include ("./menu.php");
} else {
	echo "<script>alert('Já Logado!');</script>";
	header ( "Location: index.php" );
	// include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css"
	href="./_css/style-login-cadastro.css">
<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="stylesheet" type="text/css" href="./_css/yui_combo.css">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">

<!-- Style -->
<style id="__web-inspector-hide-shortcut-style__" type="text/css">
.__web-inspector-hide-shortcut__, .__web-inspector-hide-shortcut__ *,
	.__web-inspector-hidebefore-shortcut__::
			before, .__web-inspector-hideafter-shortcut__::after {
	visibility: hidden !important;
}
</style>
</head>
<body>

	<br>
	<div class="container text-center">
		<div class="row text-center">
			<div class="col-sm-12 text-center">
				<h3>Redefinir Senha</h3>

				<form class="form-role" style="background-color: #FFF5EE;"
					action="?go=redefinir" method="post">
					<div class="container text-center">
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-label">
									<label for="email">Email</label>
								</div>
								<div class="form-input">
									<input type="email" name="email" id="email" size="30" value=""
										required />
								</div>
							</div>
						</div>
						<br>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 50%; border: 0px; padding: 0.5%"
										type="submit" class="btn-main" name="login" value="Redefinir" />
								</div>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-sm-5 text-center">
								<br>
								<p>Ou</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 30%; border: 0px; padding: 0.25%"
										type="button" class="btn-main" onclick="cancelar();"
										value="Cancelar" />
								</div>
							</div>
						</div>
						<br>
						<br>
					</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-sm-6 text-center">
							<a id="copyrights" href="Index.php">Página Inicial</a>
						</div>
					</div>
				</div>
				<br>
				<br>
				<br>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function cancelar(){
			location.href="Login.php";
		}
	</script>
</body>
<html>

<?php
include './rodape.php';

if (@$_GET ['go'] == 'redefinir') {
	
	$email = $_POST ['email'];
	
	// se campo vazio, então preenche isso
	
	// seleciona objeto da tabela usuarios, onde email for igaul ao digitado
	$conexaoEmail = $conexao->query ( "SELECT * FROM usuarios WHERE email = '$email'" )->fetch ();
	// se não existe o email nos dados, ele emite a mensagem
	if ($conexaoEmail <= 0) {
		echo "<script>alert('Email Não Cadastrado')</script>";
		// se existe o email então:
	} else {
		// seleciona objeto de usuarios onde email seja o digitado e a conta esteja ativada
		$confirma = $conexao->query ( "SELECT * FROM usuarios WHERE email = '$email' AND  ativo=1" )->fetch ();
		// se não existe, é por que a conta nao foi ativada
		if ($confirma <= 0) {
			echo "<script>alert('Conta Não Ativada!')</script>";
			// se existe, entao:
		} else {
			// cria chave criptografada para redefinir senha
			$chave = sha1 ( uniqid ( mt_rand (), true ) );
			// insere nome de usuarios e chave na tabela recupera
			$conexao->exec ( "INSERT INTO recupera (usuario, chave) VALUES ('$email', '$chave')" );
			// metodo construtor de email
			$mail = new PHPMailer ();
			// define as caractetisticas do Protocolo Simples de Envio de Emails
			$mail->IsSMTP ();
			$mail->isHTML ( true );
			// define linguagem
			$mail->Charset = 'UTF-8';
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPOptions = array (
					'ssl' => array (
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true 
					) 
			);
			// tipos de host e porta do email
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 587;
			
			// usuario e senha do remetente
			define ( 'GUSER', 'projetoptbr@gmail.com' );
			define ( 'GPWD', 'EuAmoPortugues' );
			
			$mail->Username = GUSER;
			$mail->Password = GPWD;
			
			// dados do email
			$headers = "projetoptbr@gmail.com"; // variavel que guarda o remetente
			$assunto = "Redefinir Senha"; // variavel que guarda o assunto do email
			$mensagem = "Redefinir Senha: http://localhost/Project/Arquivo/_arquivos/redefine.php?id=$email&confirmacao=$chave"; // variavem com link de recuperacao
			
			$mail->SetFrom ( $headers, "Equipe Projeto" ); // define o remetente e seu nome no email
			$mail->Subject = $assunto; // define o assunto
			$mail->Body = $mensagem; // define a mensagem do email
			
			$mail->AddAddress ( $email ); // envia o email
			
			if ($mail->Send ()) {
				// se enviado, ele retorna a mensagem
				echo "<script>alert('Enviamos Um Link em Seu Email Para Recuperação de Senha')</script>";
				//header ( "Location: Login.php" );
				echo '<meta http-equiv="refresh" content="1;URL=login.php"/>';
			} else {
				// se não enviado, retorna erro
				//echo "<script>alert('Erro')</script>";
				echo '<meta http-equiv="refresh" content="1;URL=login.php"/>';
                }
            }
        }
    }