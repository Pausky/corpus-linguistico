<?php
function padronizar_local($local){
	switch($local){
		case mb_strpos($local, ('ac' || 'acre')):
			$local = 'AC';
			break;
		case mb_strpos($local, ('al' || 'alagoas')):
			$local = 'AL';
			break;
		case mb_strpos($local, ('ap' || 'amapa' || 'amapá')):
			$local = 'AP';
			break;
		case mb_strpos($local, ('am' || 'amazonas')):
			$local = 'AM';
			break;
		case mb_strpos($local, ('ba' || 'bahia')):
			$local = 'BA';
			break;
		case mb_strpos($local, ('ce' || 'ceara' || 'ceará')):
			$local = 'CE';
			break;
		case mb_strpos($local, ('df' || 'distrito federal')):
			$local = 'DF';
			break;
		case mb_strpos($local, ('es' || 'espirito santo' ||  'espiríto santo')):
			$local = 'ES';
			break;
		case mb_strpos($local, ('go' || 'goias' || 'goiás')):
			$local = 'GO';
			break;
		case mb_strpos($local, ('ma' || 'macapa' || 'macapá')):
			$local = 'MA';
			break;
		case mb_strpos($local, ('mt' || 'mato grosso')):
			$local = 'MT';
			break;
		case mb_strpos($local, ('ms' || 'mato grosso do sul')):
			$local = 'MS';
			break;
		case mb_strpos($local, ('mg' || 'minas gerais')):
			$local = 'MG';
			break;
		case mb_strpos($local, ('pa' || 'para' || 'pará')):
			$local = 'PA';
			break;
		case mb_strpos($local, ('pb' || 'paraiba' || 'paraíba')):
			$local = 'PB';
			break;
		case mb_strpos($local, ('pr' || 'parana' || 'paraná')):
			$local = 'PR';
			break;
		case mb_strpos($local, ('pe' || 'pernambuco')):
			$local = 'PE';
			break;
		case mb_strpos($local, ('pi' || 'piaui' || 'piauí')):
			$local = 'PI';
			break;
		case mb_strpos($local, ('rj' || 'rio de janeiro')):
			$local = 'RJ';
			break;
		case mb_strpos($local, ('rn' || 'rio grande do norte')):
			$local = 'RN';
			break;
		case mb_strpos($local, ('rs' || 'rio grande do sul')):
			$local = 'RS';
			break;
		case mb_strpos($local, ('ro' || 'rondonia' || 'rondônia')):
			$local = 'RO';
			break;
		case mb_strpos($local, ('rr' || 'roraima' || 'roraíma')):
			$local = 'RR';
			break;
		case mb_strpos($local, ('sc' || 'santa catarina')):
			$local = 'SC';
			break;
		case mb_strpos($local, ('sp' || 'sao paulo' || 'são paulo')):
			$local = 'SP';
			break;
		case mb_strpos($local, ('se' || 'sergipe')):
			$local = 'SE';
			break;
		case mb_strpos($local, ('to' || 'tocantins')):
			$local = 'TO';
			break;
		default:
			break;
	}
	return $local;
}

function padronizar_mes($mes){
	switch ($mes){
		case "jan":
			$mes = '01';
			break;
		case "feb":
			$mes = '02';
			break;
		case "mar":
			$mes = '03';
			break;
		case "apr":
			$mes = '04';
			break;
		case "may":
			$mes = '05';
			break;
		case "jun":
			$mes = '06';
			break;
		case "jul":
			$mes = '07';
			break;
		case "aug":
			$mes = '08';
			break;
		case "sep":
			$mes = '09';
			break;
		case "oct":
			$mes = '10';
			break;
		case "nov":
			$mes = '11';
			break;
		case "dec":
			$mes = '12';
			break;
		default:
			$mes = 'Mes Indefinido';
			break;
	}
	return $mes;
}