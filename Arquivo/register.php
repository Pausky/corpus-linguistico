<?php
require_once "configurar.php";
require ("./APIs/phpmailer/class.phpmailer.php");
require ('./APIs/phpmailer/PHPMailerAutoload.php');
?>

<html>
<head>
<title>Cadastro | Corpus Linguistícos</title>
<?php
if (isset ( $_SESSION ["Logado"] ) == false) {
	//echo "<script>alert('Necessário Logar');</script>";
	//header ( "Location: Login.php" );
	include ("./menu.php");
} else {
	echo "<script>alert('Já Logado!');</script>";
	header ( "Location: ./index.php" );
	//include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="./estilo1.css">
<link rel="stylesheet" type="text/css" href="./_css/yui_combo.csss">
<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">

</head>
<body>

	<br>

	<div class="container text-center">
		<div class="row text-center">
			<div class="col-sm-12 text-center">
				<h3>Cadastro de Usuários</h3>

				<form class="form-role" style="background-color: #FFF5EE;"
					action="?go=cadastrar" method="post">
					<div class="container">
						<br>
						<div class="row text-center">
							<div class="col-sm-2">
								<div class="form-label">
									<label for="name">Nome</label>
								</div>
								<div class="form-input">
									<input type="text" name="name" id="name" size="27%" value=""
										required />
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-label">
									<label for="sobrenome">Sobrenome</label>
								</div>
								<div class="form-input">
									<input type="text" name="sobrenome" id="sobrenome" size="27%"
										value="" required />
								</div>
							</div>
						</div>
						<br>

						<div class="row text-center">
							<div class="col-sm-2">
								<div class="form-label">
									<label for="usernome">Usuário</label>
								</div>
								<div class="form-input">
									<input type="text" name="username" id="username" size="27%"
										value="" required />
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-label">
									<label for="password">Senha</label>
								</div>
								<div class="form-input">
									<input type="password" name="password" id="password" size="27%"
										value="" required />
								</div>
							</div>
						</div>
						<br>

						<div class="row text-center">
							<div class="col-sm-2">
								<div class="form-label">
									<label for="email">Email</label>
								</div>
								<div class="form-input">
									<input type="email" name="email" id="email" size="27%" value=""
										required />
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-label">
									<label for="repetir">Confirmar Email</label>
								</div>
								<div class="form-input">
									<input type="email" name="repetir" id="repetir" size="27%"
										value="" required />
								</div>
							</div>
						</div>
						<br>
						<br>
						<div class="row text-center">
							<div class="col-sm-2">
								<div class="form-input">
									<input style="width: 140%; border: 0px; padding: 0.8%;"
										type="submit" class="btn-main" value="Cadastrar" />
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-input">
									<input style="width: 62%; border: 0px; padding: 0.5%;"
										type="submit" class="btn-main" onclick="cancelar();"
										value="Cancelar" />
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5 text-center">
								<br>
								<p>Ou</p>

							</div>
						</div>

						<div class="row text-center">
							<div class="col-sm-5">
								<div class="forgetpass">
									<a href="./Redefinir.php">Esqueci usuário ou senha</a>
								</div>
							</div>
						</div>
						<br>

					</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-sm-6 text-center">
							<a id="copyrights" href="Index.php">Página Inicial</a>
						</div>
					</div>
				</div>
				<br>
				<br>
				<br>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	function cancelar(){
		location.href="Login.php";
	}
</script>

</body>
</html>

<?php
include 'rodape.php';

if (@$_GET ['go'] == 'cadastrar') {
	
	// pega dados dos campos
	$name = $_POST ['name'];
	$sobrenome = $_POST ['sobrenome'];
	$email = $_POST ['email'];
	$username = $_POST ['username'];
	$password = $_POST ['password'];
	$repetir = $_POST ['repetir'];
	
	// checa compatibilidade de emails digitados
	if (strcmp ( $email, $repetir ) != 0) {
		echo "<script>alert('Emails Incompatíveis')</script>";
	}
	
	if (strcmp ( $email, $repetir ) == 0) {
		$queryl = $conexao->query ( "SELECT * FROM usuarios WHERE username = '$username' OR email ='$email'" );
		$nRows = $queryl->rowCount ();
		
		// verifica se o usuario já existe
		if ($nRows >= 1) {
			echo "<script>alert('Usuário ou Email já Existe!')</script>";
		} else {
			
			// cria uma chave criptografada para ativar a conta
			$chave = sha1 ( uniqid ( mt_rand (), true ) );
			// insere dados na tabela usuarios
			$conexao->exec ( "INSERT INTO usuarios (nome, sobrenome, email, username, password, ativo, idPerfil) VALUES ('$name', '$sobrenome', '$email', '$username', '$password', 0 , '$email')" );
			// insere chave e usuario na tabela ativa
			$conexao->exec ( "INSERT INTO ativa (usuario, chave) VALUES ('$email', '$chave')" );
			
			// metodo construtor de email
			$mail = new PHPMailer ();
			$mail->MailerDebug = false;
			// define caracteristicas do email
			$mail->IsSMTP ();
			$mail->isHTML ( true );
			// define linguagem
			$mail->Charset = 'UTF-8';
			// define as caractetisticas do Protocolo Simples de Envio de Emails
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPOptions = array (
					'ssl' => array (
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
					)
			);
			// tipos de host e porta do email
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 587;
			
			// usuario e senha do remetente
			define ( 'GUSER', 'projetoptbr@gmail.com' );
			define ( 'GPWD', 'EuAmoPortugues' );
			
			$mail->Username = GUSER;
			$mail->Password = GPWD;
			
			// dados do email
			$headers = "projetoptbr@gmail.com"; // variavel que guarda o remetente
			$assunto = "Ativacao da Conta"; // variavel que guarda o assunto do email
			$mensagem = "Ativacao de Conta: http://localhost/Project/Arquivo/_arquivos/ativa.php?id=$email&confirmacao=$chave"; // variavel com o link de ativacao
			
			$mail->SetFrom ( $headers, "Equipe Projeto" ); // define o remetente e seu nome no email
			$mail->Subject = $assunto; // define o assunto
			$mail->Body = $mensagem; // define a mensagem do email
			
			$mail->AddAddress ( $email ); // envia o email
			
			if ($mail->Send ()) {
				// se enviado, ele retorna a mensagem
				echo "<script>alert('Usuário Cadastrado com Sucesso! Ative Sua Conta Via Email')</script>";
				//header ( "Location: Login.php" );
				echo '<meta http-equiv="refresh" content="1;URL=login.php"/>';
			} else {
				// se não enviado, retorna erro
				echo "Erro:" .error_log($mail->ErrorInfo);
				echo '<meta http-equiv="refresh" content="1;URL=login.php"/>';
				
			}
		}
	}
}