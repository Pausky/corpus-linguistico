#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

import SimpleHTTPServer
import SocketServer
import io
import urllib
import os
import json
import sys
import nltk
from Aelius import AnotaCorpus, Toqueniza,Extras
from Aelius import ProcessaNomesProprios
from Aelius.AnotaCorpus import anota_sentencas
from Aelius.Toqueniza import TOK_PORT_MM as tok
from Aelius.AnotaCorpus import extrai_corpus
from Aelius.Extras import carrega
from Aelius.Toqueniza import TOK_PORT_LX2 as tok



class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_GET(s):
           """Respond to a GET request."""
           s.send_response(200)
           
           s.send_header("Content-type", "text/html")
           s.end_headers()
           #s.path 

           sent = s.path[1:len(s.path)]
           sent = urllib.unquote_plus(sent).decode('utf8')
           
     

           tokens=Toqueniza.TOK_PORT.tokenize(sent)
           
           t=AnotaCorpus.anota_sentencas([tokens],AnotaCorpus.TAGGER2)
           primeiro = True
#           s.wfile.write('[')
           retorno = '['
           for w,t in t[0]:
           
              
               c="**** " + w +" **** "+t 
               if(primeiro):
                   primeiro = False
                   retorno =  retorno + '{"termo":"'+w.encode('utf-8')+'", "tag": "'+t.encode('utf-8')+'"}'
#               s.wfile.write( c.encode('utf-8').strip())
               else:
                   retorno =  retorno + ',{"termo":"'+w.encode('utf-8')+'", "tag": "'+t.encode('utf-8')+'"}'

           retorno =  retorno + ']'
           s.wfile.write(retorno)
           
           return


print('Server listening on port 8000...')
httpd = SocketServer.TCPServer(('', 8000), Handler)
httpd.serve_forever()
