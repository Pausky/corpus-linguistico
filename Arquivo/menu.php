<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Menu Deslogado</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon"
	href="http://icon-icons.com/icons2/317/PNG/512/book-bookmark-icon_34486.png">

</head>
<body>

	<!-- menu horizontal -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand img-responsive" href="./index.php"> <img
					src="./_css/_img/Logo.png" width="80%" alt="logo">
				</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="link dropdown-toggle" data-toggle="dropdown">Descubra<b
							class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="./projeto.php" class="link">O que é o Projeto PT-br</a></li>
							<!-- <li><a href="./captura.php" class="link">Capturas de tela</a></li>-->
						</ul></li>
					<li><a class="link dropdown-toggle" data-toggle="dropdown">Acesse
							Já<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
							<li><a href="./buscar.php" class="link">Busca ao Corpus</a></li>
							<li><a href="./aovivo.php" class="link">Busca ao Vivo</a></li>
							<!-- <li><a href="./recursos.php" class="link">Recursos</a></li>-->
							<li><a href="./documentacao.php" class="link">Documentação</a></li>
						</ul></li>
					<li class=""><a class="link dropdown-toggle" data-toggle="dropdown">Ajuda<b
							class="caret"></b></a>
						<ul class="dropdown-menu">
							<!--<li><a href="./discussao.php" class="link">Listas de discussão</a></li>-->
							<!--<li><a href="./forum.php" class="link">Fórum</a></li>-->
							<!--<li><a href="./suporte.php" class="link">Suporte</a></li>-->
							<li><a href="./bugs.php" class="link">Assistente de bugs</a></li>
						</ul></li>
					<li><a class="link dropdown-toggle" data-toggle="dropdown">Sobre
							nós<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
							<li><a href="./nos.php" class="link">Quem somos</a></li>
							<!--<li><a href="./creditos.php" class="link">Créditos</a></li>-->
							<li><a href="./contato.php" class="link">Contato</a></li>
						</ul></li>
					<li><a href="./login.php" class="link">Login</a></li>
				</ul>
			</div>
		</div>
	</div>

	<header>
		<br></br> <br></br> <br></br>
		<p id="logo">Corpus Linguístico</p>
		<br></br>
	</header>

</body>
</html>