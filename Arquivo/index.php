<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Home | Projeto PT-br</title>
<?php 
	// inicia a sessão
	session_start ();
	// se a sessão for falsa, volta pro login
	if (isset ( $_SESSION ["Logado"] ) == false) {
		include("./menu.php");
	}else{
		include("./menuLogado.php");
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css" href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css" href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css" href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">


</head>

<body class="Homepage no-sidebar" id="home">

	<br>
	<br>
	<!-- icones -->
	<section id="highlights" class="section">
		<div class="container">
			<div class="row text-center homepage">
				<div class="col-sm-12 margin-20">
					<h3>IFSC e a Tecnologia</h3>
					<br> <br>
					<p>O Projeto PT-br é um projeto de pesquisa do Instituto Federal de
						Santa Catarina (IFSC)</p>
					<br>
					<p>O projeto integra a Linguistíca e a Computação, a fim de
						desenvolver um corpus linguistíco do português não padrão</p>
					<br>
					<p>O projeto engloba redes sociais, blogs e sites de notícias, o
						que o torna um corpus mais completo</p>
					<br> <br>
				</div>

				<!--Section 1-->
				<div class="col-sm-4 service margin-20">
					<a><img src="./_css/_img/1.png" width="30%"
						alt="icone-documento">
					</a>
					<h3>Objetivos</h3>
					<p>O objetivo do projeto é criar um corpus do português brasileiro
						não padrão, que inclua entre 1 milhão e 10 milhões de palavras</p>
				</div>
				<!--Section 2-->
				<div class="col-sm-4 service margin-20">
					<a><img src="./_css/_img/2.png" width="30%" alt="icone-polegar">
					</a>
					<h3>Um Projeto de Socialidade</h3>
					<p>O Projet PT-br é mais que um corpus. É também gente, cultura e
						sociedade</p>
					<p>
						<a href="./projeto.php" class="green bold">Descubra o Projeto!</a>
					</p>
				</div>
				<!--Section 3-->
				<div class="col-sm-4 service margin-20">
					<a><img src="./_css/_img/3.png" width="30%" alt="icone-asteristico"> </a>
					<h3>Inovação</h3>
					<p>O Projeto PT-br é o primeiro corpus linguístico à dar ênfase à
						variação da língua portuguesa não padrão no Brasil</p>
				</div>
			</div>
		</div>
	</section>
	<!-- passa imagens -->
	<section class="section">
		<div id="header" class="carousel slide animated fadeIn"
			data-ride="carousel" style="background-color: black">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#header" data-slide-to="0" class="active"></li>
				<li data-target="#header" data-slide-to="1" class=""></li>
				<li data-target="#header" data-slide-to="2" class=""></li>
			</ol>
			<div class="carousel-inner">
				<div class="item one">
					<div class="container animated fadeInUp">
						<div class="carousel-caption">
							<h1 style="color: #000000">Corpus Para a Comunidade</h1>
							<p class="lead margin-40" style="color: #000000">
								<em>Projeto PT-br: o corpus que a comunidade linguistíca sonhou
									por anos ter.</em>
							</p>
							<a style="color: #000000" class="btn-home" href="./projeto.php"><i
								class="icon-chevron-right"></i>Conheça Já</a>
						</div>
					</div>
				</div>
				<div class="item two active">
					<div class="container">
						<div class="carousel-caption">
							<h1 style="color: #000000">Gente Representada</h1>
							<p style="color: #000000" class="lead margin-40">
								<em>O Projeto PT-br não é apenas uma ferramenta de busca, é o
									registro linguistíco da sociedade atual</em>
							</p>
							<a style="color: #000000" class="btn-home" href="./buscar.php"><i
								class="icon-chevron-right"></i>Realize uma Busca</a>
						</div>
					</div>
				</div>
				<div class="item three">
					<div class="container">
						<div class="carousel-caption">
							<h1 style="color: #000000">Um Projeto do IFSC</h1>
							<p style="color: #000000" class="lead margin-40">
								<em>O Projeto PT-br é um projeto de pesquisa do Instituto
									Federal de Santa Catarina</em>
							</p>
							<a style="color: #000000" class="btn-home"
								href="https://www.ifsc.edu.br"><i class="icon-chevron-right"></i>Conheça
								o IFSC</a>
						</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control hidden-xs" href="/#header"
				data-slide="prev"><i class="fa fa-angle-left"></i></a> <a
				class="right carousel-control hidden-xs" href="/#header"
				data-slide="next"><i class="fa fa-angle-right"></i></a>
		</div>
	</section>
	<!-- Frase sobre o Projeto -->
	<section id="quote" class="section">
		<div class="container">
			<div class="row">
				<div style="positon-left: 10px;" class="col-sm-3 margin-30">
					<img src="./_css/_img/doutora.png" alt="Dra. Caroline Reis"
						style="width: 100%;">
				</div>
				<div class="col-sm-9">
					<h2 style="color: #000000;">"O Projeto é uma inovação, uma vez que
						os corpus que tratam do português brasileiro, não possuem nenhuma
						abordagem à variação regional da língua, as conhecidas gírias" -
						Dra. Caroline Reis</h2>
					<br> <a
						style="font-size: 20px; border: 10px solid green; background-color: green; color: #FFFFFF; font-family: cursive;"
						href="./projeto.php">Quero Conhecer!</a>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-20 text-center">
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
				</div>
			</div>
		</div>

	</section>

</body>
</html>
<?php require 'rodape.php';