<?php
	//ao acessar este arquivo, ele destroi a sessão, deslogando a pessoa
	session_start();
	unset($_SESSION["Logado"]);
	unset($_SESSION["nome"]);
	unset($_SESSION["sobrenome"]);
	unset($_SESSION["email"]);
	unset($_SESSION["username"]);
	unset($_SESSION["password"]);
	session_destroy();
	
    header("Location: index.php");

?>