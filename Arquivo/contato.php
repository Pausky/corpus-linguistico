<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Contato | Projeto PT-br</title>
<?php
// inicia a sessão
session_start ();
// se a sessão for falsa, volta pro login
if (isset ( $_SESSION ["Logado"] ) == false) {
	include ("./menu.php");
} else {
	include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">


</head>

<body>
	<br>
	<div class="container">
		<div class="row text-text-left">
			<div class="col-sm-12 text-center">
				<h3 class="text-success text-center">Contato</h3>
				<br>
				<div class="col-sm-12 text-center">
					<div style="float: right;" class="row-fluid">
						<div class="span4 text-right">
							<div class="contact-info">
								<p>
									Campus Gaspar:<br> Rua Adriano Kormann, 510 - <br>Bela Vista
									Gaspar - SC<br> CEP: 89110-971<br> <br> <i
										class="fa fa-phone-square"></i>Telefone: (47) 3318-3700<br> <i
										class="fa fa-envelope"></i>E-mail: ProjetoPTbr@gmail.com<br> <a
										href="#localizacao">Ver Localização</a>
								</p>
							</div>
						</div>


					</div>
					<div style="float: left" class="span3 text-left">
						<div class="infoarea">
							<div class="footer-logo">
								<img src="./_css/_img/ifsc.png" width="100%" alt="Logo do IFSC"><br>
							</div>
						</div>
					</div>
					<form action="?go=contatar" method="post"
						style="background-color: #FFF5EE; width: 190%">
						<br>
						<div class="row text-left">
							<div class="col-sm-5">
								<div class="row text-left">
									<div class="col-sm-5">
										<div class="form-label">
											<label for="username">Nome:</label>
										</div>
										<div class="form-input">
											<input type="text" name="nome" id="nome" size="30"
												value="<?php
												// inicia a sessão
												
												// se a sessão for falsa, volta pro login
												if (isset ( $_SESSION ["Logado"] ) == false) {
													// echo "<script>alert('Necessário Logar');</script>";
													// header ( "Location: Login.php" );
													echo "";
												} else {
													echo "" . $_SESSION ['nome'] . " " . $_SESSION ['sobrenome'];
												}
												?>"
												required />
										</div>
									</div>
								</div>
								<div class="row text-left">
									<div class="col-sm-5">
										<div class="form-label">
											<label for="password">Email:</label>
										</div>
										<div class="form-input">
											<input type="email" name="email" id="email" size="30"
												value="<?php
												// inicia a sessão
												
												// se a sessão for falsa, volta pro login
												if (isset ( $_SESSION ["Logado"] ) == false) {
													// echo "<script>alert('Necessário Logar');</script>";
													// header ( "Location: Login.php" );
													echo "";
												} else {
													echo "" . $_SESSION ['email'] . "";
												}
												?>"
												required />
										</div>
									</div>
								</div>
								<br>
								<div class="row text-left">
									<div class="col-sm-5">
										<div class="form-label">
											<label for="username">Assunto:</label>
										</div>
										<div class="form-input">
											<input type="text" name="assunto" id="assunto" size="30"
												value="" required />
										</div>
									</div>
								</div>
								<br>
								<div class="row text-left">
									<div class="col-sm-12">
										<div class="form-label">
											<label for="mensagem">Mensagem: </label>
										</div>
										<div class="form-input">
											<textarea rows="5" cols="60" name="mensagem" id="mensagem"
												required></textarea>
										</div>
									</div>
								</div>
								<br>
								<div class="row text-left">
									<div class="col-sm-12">
										<div class="form-input">
											<input
												style="width: 100%; border: 0px; padding: 0.7%; margin-left: 3%; display: inline;"
												type="submit" class="btn-main" name="enviar" value="enviar" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
					</form>
				</div>
				<section id="localizacao" class="secaocolorida">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="page-header">
									<h3>Localização</h3>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<iframe
									src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAOp186f2Z7cL9pl_TdVIGQi2oihwsMI68&q=IFSC+Gaspar,Gaspar+SC"
									width="100%" height="450" frameborder="0" style="border: 0"
									allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function cancelar(){
		location.href="index.php";
	}
	</script>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

</body>
</html>
<?php

$keyAPI = "AIzaSyAB2DmJOgF29J2v2NaKw4U02w2ZPLRJYzI";
require 'rodape.php';
require ("./APIs/phpmailer/class.phpmailer.php");
require ('./APIs/phpmailer/PHPMailerAutoload.php');

if (@$_GET ['go'] == 'contatar') {
	
	// pega dados dos campos
	$name = $_POST ['nome'];
	$email = $_POST ['email'];
	$mensagem = $_POST ['mensagem'];
	$assunto = $_POST ['assunto'];
	
	// metodo construtor de email
	$mail = new PHPMailer ();
	$mail->MailerDebug = false;
	// define caracteristicas do email
	$mail->IsSMTP ();
	$mail->isHTML ( true );
	// define linguagem
	$mail->Charset = 'UTF-8';
	// define as caractetisticas do Protocolo Simples de Envio de Emails
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPOptions = array (
			'ssl' => array (
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true 
			) 
	);
	// tipos de host e porta do email
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	
	// usuario e senha do remetente
	define ( 'GUSER', 'projetoptbr@gmail.com' );
	define ( 'GPWD', 'EuAmoPortugues' );
	
	$mail->Username = GUSER;
	$mail->Password = GPWD;
	
	// dados do email
	$headers = "projetoptbr@gmail.com"; // variavel que guarda o remetente
	$assunt = "Contato para: $assunto"; // variavel que guarda o assunto do email
	$msg = "Nome: $name <br> Email: $email <br> Mensagem: $mensagem"; // variavel com o link de ativacao
	
	$mail->SetFrom ( $headers, "Email Contato" ); // define o remetente e seu nome no email
	$mail->Subject = $assunt; // define o assunto
	$mail->Body = $msg; // define a mensagem do email
	
	$mail->AddAddress ( "projetoptbr@gmail.com" ); // envia o email
	
	if ($mail->Send ()) {
		// se enviado, ele retorna a mensagem
		echo "<script>alert('Email Enviado')</script>";
		header ( "Location: contato.php" );
	} else {
		// se não enviado, retorna erro
		echo "Erro:" . error_log ( $mail->ErrorInfo );
	}
}