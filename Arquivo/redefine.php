<?php
// requer os arquivos de conexao ao DB e para enviar email
require_once ('./configurar.php');
require ("./APIs/phpmailer/class.phpmailer.php");
require ('./APIs/phpmailer/PHPMailerAutoload.php');
?>

<html>
<head>
<title>Redefinição de Senha | Corpus Linguístico</title>
<?php
session_start ();

if (isset ( $_SESSION ["Logado"] ) == false) {
	// echo "<script>alert('Necessário Logar');</script>";
	// header ( "Location: Login.php" );
	include ("./menu.php");
} else {
	echo "<script>alert('Já Logado!');</script>";
	header ( "Location: ./index.php" );
	// include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css"
	href="./_css/style-login-cadastro.css">
<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="stylesheet" type="text/css" href="./_css/yui_combo.css">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">


</head>
<body>

	<br>
	<div class="container text-center">
		<div class="row text-center">
			<div class="col-sm-12 text-center">
				<h3>Redefinir Senha</h3>

				<form class="form-role" style="background-color: #FFF5EE;"
					action="?go=redefinir" method="post">
					<div class="container text-center">
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-label">
									<label for="senha">Nova Senha</label>
								</div>
								<div class="form-input">
									<input type="password" name="senha" id="senha" size="30"
										maxlength="99" value="" required />
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-label">
									<label for="senha">Confirmação Nova Senha</label>
								</div>
								<div class="form-input">
									<input type="password" name="repetir" id="repetir" size="30"
										maxlength="99" value="" required />
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 50%; border: 0px; padding: 0.5%"
										type="submit" class="btn-main" name="login" value="Redefinir" />
								</div>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-sm-5 text-center">
								<br>
								<p>Ou</p>
								<br>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 30%; border: 0px; padding: 0.25%"
										type="button" class="btn-main" onclick="cancelar();"
										value="Cancelar" />
								</div>
							</div>
						</div>
						<br>
					</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-sm-6 text-center">
							<a id="copyrights" href="Index.php">Página Inicial</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function cancelar(){
			location.href="Login.php";
		}
	</script>
</body>
<html>


<?php
if (! empty ( $_POST )) {
	// pega dados do link
	$usuario = @$_GET ['id'];
	$hash = @$_GET ['confirmacao'];
	
	$password = $_POST ['senha'];
	$repetir = $_POST ['repetir'];
	
	// se campos de senhas tiverem dados diferentes, emite mensagem
	if (strcmp ( $password, $repetir ) !== 0) {
		echo "<script>alert('Senhas Diferentes')</script>";
		echo '<meta http-equiv="refresh" content="1;URL=redefine.php"/>';
	}
	// se campos de senha forem iguais
	if (strcmp ( $password, $repetir ) == 0) {
		// seleciona objeto da tabela usuarios onde email e senha forem iguais aos digitados
		$conect = $conexao->query ( "SELECT * FROM usuarios WHERE email = '$usuario' AND password ='$password'" )->fetch ();
		
		if ($conect >= 1) { // se for igual é prq é a mesma senha que a antiga
			echo "<script>alert('Mesma Senha Que a Antiga!')</script>";
		} else {
			// se for nova senha, ele seleciona objeto da tabela recupera onde o usuario a chave são iguais à do link
			$q = $conexao->query ( "SELECT * FROM recupera WHERE usuario = '$usuario' AND chave='$hash'" )->fetch ();
			
			// se existe objeto com aquelas caracteristicas, ele inicia a outra parte
			if ($q >= 1) {
				// muda em usuarios a senha, onde a coluna email for igual ao emial do link
				$conexao->exec ( "UPDATE usuarios SET password = '$password' WHERE email = '$usuario'" );
				// deleta o objeto em recupera onde o usuario e a chave forem iguais ao do link
				$conexao->exec ( "DELETE FROM recupera WHERE usuario = '$usuario' AND chave = '$hash'" );
				// seleciona objeto na table usuario, onde senha e email sao iguais aos campos digitados (password) e do link (email)
				$conn = $conexao->query ( "SELECT * FROM usuarios WHERE password = '$password' AND email = '$usuario'" )->fetch ();
				
				// se existe esse objeto, emite a mensagem
				if ($conn >= 1) {
					echo "<script>alert('Senha Atualizada!')</script>";
					echo '<meta http-equiv="refresh" content="1;URL=login.php"/>';
					// se não existe esse obejto, deu erro interno (programacao)
				} else {
					echo "<script>alert('Erro Interno! Contate ao Proprietário do Site!')</script>";
				}
				// se não existe objeto com usuario e chave do link,entoao o link é inválido
			} else {
				echo "<script>alert('Link Inválido!')</script>";
	 			}
	 		}
	 	}
	}

?>