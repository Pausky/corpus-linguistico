<!DOCTYPE html>

<footer  style="width=100%;" class="footer"  id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 text-center">
					<div style="float: left" class="span3 text-left">
						<div class="infoarea">
							<div class="footer-logo">
								<a href="#"><img src="./_css/_img/ifsc.png" width="100%"
									alt="Logo do IFSC"></a>
							</div>
						</div>
					</div>
					<a style="border: 2px solid green" class="btn btn-_green"
						href="./index.php" >Início</a> <a
						style="border: 2px solid green" class="btn btn-_green"
						href="./suporte.php" >Suporte</a> <a
						style="border: 2px solid green" class="btn btn-_green"
						href="./contato.php" >Contato</a> <a
						style="border: 2px solid green" class="btn btn-_green"
						href="./projeto.php" >Projeto</a>

					<div style="float: right;" class="row-fluid">
						<div class="span4 text-right">
							<div class="contact-info">
								<h3 class="nopadding">Contato</h3>
								<p>
									Campus Gaspar:<br> Rua Adriano Kormann, 510 - Bela Vista <br>
									Gaspar - SC<br> CEP: 89110-971<br> <br> <i
										class="fa fa-phone-square"></i>Telefone: (47) 3318-3700<br> <i
										class="fa fa-envelope"></i>E-mail: ProjetoPTbr@gmail.com<br>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<h3>Siga-nos nas Redes Sociais:</h3>
					<ul class="social-icons">
						<li>
							<a class="btn btn-_green" href="https://twitter.com/riikivargas" target="_blank">
								<img src="./_css/_img/twitter.png" width="40" alt="twitter">
							</a>
						</li>
						<li>
							<a class="btn btn-_green" href="https://www.facebook.com/riikivargas" target="_blank">
								<img src="./_css/_img/facebook.png" width="40" alt="facebook">
							</a>
						</li>
						<li>
							<a class="btn btn-_green" href="https://plus.google.com/+RicardoVargasOficial/" target="_blank">
								<img src="./_css/_img/google.png" width="40" alt="google+">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

