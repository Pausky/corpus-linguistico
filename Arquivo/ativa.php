<?php
//requer conexao com o DB
require('configurar.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title> Ativação de Conta</title>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
</head>
<body>

<?php
//pega dados do link
$id = @$_GET['id'];
$hash = @$_GET['confirmacao'];

//verifica se o usuário ja está ativo ou não
$confirma = $conexao->query("SELECT * FROM usuarios WHERE email = '$id' AND  ativo=1")->fetch();

if ($confirma>=1) {
	echo "<script>alert('Conta Já Ativa!')</script>";
	header("Refresh:0; url=Login.php");
}else{
	//seleciona objeto da tabela ativa se o usuario e chave do link estivem na tabela
	$q = $conexao->query("SELECT * FROM ativa WHERE usuario = '$id' AND chave='$hash'")->fetch();

	//se esta na tabela
	if ($q>=1) {
		//muda a coluna ativo para 1 (ativada a conta) onde a coluna email for igual ao email do link
		$conexao->exec("UPDATE usuarios SET ativo = 1 WHERE email = '$id'");
		//deleta o objeto da tabela ativa, se o usuario e chave forem as mesmas do link
	 	$conexao->exec("DELETE FROM ativa WHERE usuario = '$id' AND chave = '$hash'");
	 	//seleciona o objeto da tabela usuarios se o a coluna email for gual ao do link e estiver ativo(1)
		$final = $conexao->query("SELECT * FROM usuarios WHERE email = '$id' AND ativo=1")->fetch();

		if ($final>=1) {
			//se existe um objeto assim é por que ativou a conta
			echo "<script>alert('Conta Ativada!')</script>";
			header ( "Location: Login.php" );
		}else{
			//se não, é erro na programação
			echo "<script>alert('Erro Interno! Contate ao Proprietário do Site!')</script>";
		}
	 				
	}else{
		//caso a chave e o usuario no link nao forem iguais à nenhum dado na tabela ativa, é link inválido
	 	echo "<script>alert('Link Inválido!')</script>";
	}
}
?>

</body>
</html>