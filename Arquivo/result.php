﻿<?php
session_start();

if (isset ( $_SESSION ["Logado"] ) == false) {
	echo "<script>alert('Necessário Logar');</script>";
	header ( "Location: Login.php" );
	//include ("./menu.php");
} else {
	include ("./menuLogado.php");
}
//require_once 'configurar.php';
//require_once 'Apache/Solr/Service.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Results of Query</title>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
</head>

<body>

<?php

	//pega dados do link
	$queries = @$_GET['q'];
	$local = @$_GET['local'];
	$origem = @$_GET['origem'];
	$inicio = @$_GET['inicio'];
	$fim = @$_GET['fim'];

	//chaves de conexão no Solr
	define('SOLR_SERVER_HOSTNAME', 'localhost');
	define('SOLR_SERVER_USERNAME', 'admin');
	define('SOLR_SERVER_PASSWORD', '');
	define('SOLR_SERVER_PORT', 8983);

	//Conexão com o Solr
	$client = new Apache_Solr_Service(SOLR_SERVER_HOSTNAME, SOLR_SERVER_PORT,'/solr/corpus_twitter');
	$client->setAuthenticationCredentials(SOLR_SERVER_USERNAME, SOLR_SERVER_PASSWORD );
		
	$offset = 0;
	$limit = 20;

	$data0 = "$inicio.T00:00:00Z";
	$date1 = "$fim.T00:00:00Z";

	//protocolo de pesquisa
	$response = $client->search($queries, $offset, $limit,$local, $origem, $data0, 'local', 'origem', 'data');

	if ( $response->getHttpStatus() == 200 ) {
		
		if ( $response->response->numFound > 0 ) {
			?>
			
			<p id="logo">Resultados</p><br>
			
			<table style="align:center; border:2;">
			
			<thead align="center">
    			<tr>
    				<th style="width:50px;">Anterior</th>
    				<th style="width:50px;">Palavra</th>
    				<th style="width:50px; colspan:2px;">Posterior</th>
    			<tr>
			</thead>

			<?php
			foreach ( $response->response->docs as $doc) {
				$texto = $doc->texto;
				
				$tamanhoQuery = mb_strlen($queries);//retorna o tamanho da string (tamanho da palavra pesquisada)
				$posicao = mb_strpos($texto, $queries);//retorna a posicao da primeira ocorrencia da string

				$pre = mb_substr($texto, 0, $posicao);//retorna string desde o inicio até a posicao da palavra pesquisada
				$now = mb_substr($texto, $posicao, $tamanhoQuery);//retorna string da palavra pesquisada
				$pos = mb_substr($texto, $posicao+$tamanhoQuery);//retorna string desde a posicao da palavra pesquisada até o fim
			?>
				<tbody>
				<tr>
					<td><?php echo "$pre"; ?></td>
					<td><?php echo "$now"; ?></td>
					<td>
						<div style="float:left" class="div-em-colunas">
							<?php echo "$pos"; ?>
						</div>
						<div style="float:right" class="div-em-colunas">
							<span class="help-note-title"></span>
						</div>
					</td>
				<tr>
				</tbody>

				<?php
				$local = $doc->local;
				$data = $doc->data;
				$origem = $doc->origem;
				?>

				<thead>
				<tr>
					<th>Dado</th>
					<th colspan="2">Conteúdo</th>
				</tr>
				</thead>

				<tbody>
				<tr>
					<td> <?php echo "$local" ?>;</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td> <?php echo "$data" ?>;</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td> <?php echo "$origem" ?>;</td>
					<td colspan="2"></td>
				</tr>

				<?php
				while($posicao < mb_strrpos($texto, $queries)){//realiza até que a posicao da proxima ocorencia da palavra pesquisada, seja a ultima
					
					//pega posicao da próxima ocorrencia em toda a frase pesquisada
					$posicao = mb_strlen($pre) + $tamanhoQuery + mb_stripos($pos, $queries);

					$pre = mb_substr($texto, 0, $posicao);//retorna string desde o inicio até a próxima posicao da palavra pesquisada
					$now = mb_substr($texto, $posicao, $tamanhoQuery);//retorna string da palavra pesquisada
					$pos = mb_substr($texto, $posicao+$tamanhoQuery);//retorna string da palavra pesquisada até o fim
				?>
					<tr>
						<td><?php echo "$pre"; ?></td>
						<td><?php echo "$now"; ?></td>
						<td><?php echo "$pos"; ?></td>
					</tr>
					</tbody>
				<?php

				}
			}
			?>

			</table>
			<?php

		}else{
			?><hr><?php
			echo "Sorry, No Results";
		}
		
	}else {
		//se não ter resposta, ele retorna a mensagem
		echo "$response->getHttpStatusMessage()";
	}
?>
</body>
</html>