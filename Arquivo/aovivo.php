<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Busca Ao Vivo | Projeto PT-br</title>
<?php
// inicia a sessão
session_start ();
// se a sessão for falsa, volta pro login
if (isset ( $_SESSION ["Logado"] ) == false) {
	echo "<script>alert('Necessário Logar');</script>";
	header ( "Location: login.php" );
}else{
	include("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="./_css/table-result.css">

<script type="text/javascript" src="./_scripts/table-result.js"></script>

<link rel="stylesheet" type="text/css" href="./_css/yui_combo.csss">
<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">
	<script type="text/javascript" src="./_scripts/jquery-1.10.1.min.js.download"></script>
	<script type="text/javascript" src="./_scripts/bootstrap.min.js.download"></script>

</head>

<body class="Homepage no-sidebar" id="home">

	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12">
				<br> <br>

				<div class="row text-center">
					<div class="col-sm-12">
						<div class="form-label">
							<h3><?php echo "Welcome " . $_SESSION ["nome"] . " " . $_SESSION ["sobrenome"]; ?></h3>
						</div>
					</div>
				</div>
				<br> <br>

				<form class="form-role" style="background-color: #FFF5EE;"
					action="?go=buscar" method="post">
					<br>
					<div class="row text-center">
						<div class="col-sm-12">
							<div class="form-label">
								<label for="palavra_chave">Palavra-Chave</label>
							</div>
							<div class="form-input">
								<input type="text" name="palavra_chave" id="palavra_chave"
									size="60" value="" required />
							</div>
						</div>
					</div>
					<br>

					<div class="row text-center">
						<div class="col-sm-12">
							<div class="form-input">
								<input style="width: 25%; border: 0px; padding: 1%"
									type="submit" class="btn-main" name="login" value="Buscar" />
							</div>
						</div>
					</div>
					<br><br>
				</form>
				<br> <br>
				<?php require "./scriptAoVivo.php"?>
			</div>
		</div>
	</div>
<?php require 'rodape.php'; ?>
</body>
</html>



