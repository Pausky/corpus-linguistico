<?php
require 'configurar.php';
// Aqui vai exibir o perfil
session_start ();
$username = $_SESSION ['username'];

$queryID = $conexao->query ( "SELECT * FROM usuarios WHERE username  = '$username'" /* AND ativo=1"*/ )->fetch ();
if ($queryID > 0) {
	$idPerfil = $queryID ['idPerfil'];
	echo "$idPerfil";
}
$query = $conexao->query ( "SELECT idPerfil FROM perfil WHERE idPerfil  = '$idPerfil'" )->fetch ();

?>
<html>
<head>
<title><?php echo "" .$_SESSION['nome'] ." " .$_SESSION['sobrenome']?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">

</head>
<body>
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Nome: <?php echo "" . $_SESSION ['nome'] . "<br>"; ?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Sobrenome: <?php echo "" . $_SESSION ['sobrenome'] . "<br>"; ?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Biografia: <?php echo "" . $query ['biografia'] . "<br>"; ?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Ensino Fundamental: <?php echo "" . $query ['ensino_basico'] . "<br>";
				?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Ensino Médio: <?php echo "" . $query ['ensino_medio'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Ensino Superior: <?php echo "" . $query ['ensino_superior'] . "<br>";?>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Cidade Atual: <?php echo "" . $query ['cidade_atual'] . "<br>";?>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Cidade Natal: <?php echo "" . $query ['cidade_natal'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Relacionamento: <?php echo "" . $query ['relacionamento '] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Perfil Criado em: <?php echo "" . $query ['create_at'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Habilidades Profissionais: <?php echo "" . $query ['habilidade_prof'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Profissão: <?php echo "" . $query ['trabalho'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Celular: <?php echo "" . $query ['celular'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Data Nascimento: <?php echo "" . $query ['nasc'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Genero <?php echo "" . $query ['genero'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Religião <?php echo "" . $query ['religiao'] . "<br>";?></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<div class="form-label">Citação Favorita <?php echo "" . $query ['citacao'] . "<br>";?></div>
			</div>
		</div>
	</div>
</body>
</html>