<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>O Projeto | Projeto PT-br - O corpus do português não padrão</title>
<?php
session_start ();

if (isset ( $_SESSION ["Logado"] ) == false) {
	// echo "<script>alert('Necessário Logar');</script>";
	// header ( "Location: Login.php" );
	include ("./menu.php");
} else {
	include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description"
	content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css"
	href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css"
	href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css"
	href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon"
	href="http://icon-icons.com/icons2/317/PNG/512/book-bookmark-icon_34486.png">

</head>

<body class="Homepage no-sidebar" id="home">

	<br>
	<br>
	<!-- icones -->
	<section id="highlights" class="section">
		<div class="container">
			<div class="row text-left homepage">
				<div class="col-sm-12">
					<h3 class="text-success text-center">O que é o Projeto?</h3>
					<br>
					<p>O Projeto Pt-br é um projeto de pesquisa do Instituto Federal de
						Santa Catarina e este projeto surgiu da demanda dos profissionais
						de linguística, tendo como objetivo desenvolver um corpus
						linguístico do português brasileiro não-padrão.</p>
					<p>O corpus linguístico supracitado, compreende uma área da
						Linguística que atua na coleta e exploração de conjunto de dados
						linguísticos coletados criteriosamente com o objetivo de servirem
						para pesquisa de uma língua e suas variações, a Linguística de
						Corpus. O conjunto de dados linguísticos coletados neste projeto,
						correspondem aos fenômenos típicos da variante não padrão da
						língua e da oralidade, as conhecidas gírias.</p>
					<p>Apesar de já existir outros corpus linguísticos do português
						brasileiro, nenhum tratara desses fenômenos (gíria, regionalidade,
						entre outros), portanto, fora através de uma demanda da área em si
						a criação desse corpus, necessário para possibilitar aos
						profissionais da linguistíca a descrição do funcionamento de
						estruturas da língua não padrão e ainda, trazer vantagens para a
						prática de tradutores, lexicógrafos e outros estudiosos.</p>
					<p>Especializado em diferentes gêneros textuais retirados de redes
						sociais, blogs e sites de notícias populares, a ideia do corpus é
						que este tenha uma dimensão de entre 1 milhão e 10 milhões de
						palavras, sendo que este, como já explicitado, composto por gírias
						e váriações da língua.</p>
					<p>Além de desenvolver o corpus, é necessário disponibilizá-lo ao
						público e para isso foram desenvolvidas as páginas deste site.</p>
					<p>Este site abrange um sistema de busca conectado ao corpus e já
						possibilita a consulta de palavras e frases, ainda assim, este
						sistema só retorna os textos que possuem a frase ou palavra
						pesquisada, de modo que esta fique centralidada e sinalizada na
						tela.</p>
					<p>Por isso é necessário etiquetar as palavras contidas no corpus,
						de modo que se possa existir uma consulta e retorno mais rico
						linguisticamente.</p>

					<p>Para esta etiquetação, criou-se outro projeto, que é uma
						extensão do anterior, o Projeto de Estudo dos Etiquetadores
						Linguistícos, que é um projeto que visa analisar os etiquetadores
						atuais e se possível adaptá-los ao corpus, desenvolvendoo um
						etiquetador próprio para as gírias.</p>
					<p>O uso dessas etiquetas nos textos armazenados em um corpus ou
						corpora facilitaria o trabalho de pesquisadores, tendo em vista
						que possibilitaria a realização de buscas mais específicas no
						corpus, portanto cabe à este projeto atual, tal qual é seu
						objetivo, o estudo e desenvolvimento de um etiquetador automático
						de textos do português brasileiro não padrão.</p>
					<p>A ideia primária e pensada é que esse etiquetador receberá um
						texto em linguagem natural, na língua portuguesa, e retornará uma
						lista com as palavras da entrada associadas as suas categorias
						morfológicas.</p>
					<p>Como o principal diferencial e foco do projeto, encontra-se a
						habilidade de etiquetar textos exclusivamente em manifestações não
						padrão da língua. Ao final desta etapa, espera-se etiquetar o
						corpus desenvolvido na etapa anterior do projeto, enriquecer o
						sistema de busca ao corpus e disponibilizá-lo ao público.</p>
				</div>
			</div>
		</div>
	</section>


</body>
</html>
<?php

include 'rodape.php';
