<?php


// pega arquivos
require_once 'configurar.php';

$params = array ();

?>
<html>
<head>
<title>Busca Corpus Linguistícos</title>
<?php
// inicia a sessão
session_start ();
// se a sessão for falsa, volta pro login
if (isset ( $_SESSION ["Logado"] ) == false) {
	echo "<script>alert('Necessário Logar');</script>";
	header ( "Location: login.php" );
} else {
	include ("./menuLogado.php");
}
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="./_css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./_css/main.css">
<link rel="stylesheet" type="text/css" href="./_css/flexslider.css">
<link rel="shortcut icon" href="http://icon-icons.com/icons2/317/PNG/512/book-bookmark-icon_34486.png">

</head>
<body>
	<div class="container-form">
		<div class="row text-center">
			<div class="col-sm-12">
				<br> <br> <br>
				<h3>Busca no Corpus</h3>
				<form class="form-role" style="background-color: #FFF5EE;"
					name="form1" action="?go=buscar">
					<br>
					<div class="container text-center">
						<div class="row text-center">
							<div class="col-sm-8">
								<div class="form-label">
									<label for="busca">Campo de Busca </label>
								</div>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-sm-9">
								<div class="form-input" style="display: inline-block;">
									<input style="display: inline-block; width: 150%;" name="q"
										id="q" type="text" required />
								</div>
								<div style="display: inline-block;">
									<input style="display: inline-block; margin-left: 60%;"
										type="submit" name="submit" value="buscar">
								</div>
							</div>
						</div>
						<hr>
						<div class="row text-center">
							<div class="col-sm-9" style="display: inline-block;">
								<div style="display: inline-block;">
									<p>Filtros:</p>
								</div>
								<div style="margin-left: 5%; display: inline;"
									class="form-input">
									<input type="checkbox" name="opcao" id="local" value="local"
										onclick="exibe_filtros(0);">
								</div>
								<div style="display: inline;" class="form-label">
									<label for="local">Local</label>
								</div>
								<div style="display: inline; margin-left: 5%;"
									class="form-input">
									<input type="checkbox" name="opcao" id="origem" value="origem"
										onclick="exibe_filtros(1);">
								</div>
								<div style="display: inline;" class="form-label">
									<label for="origem">Origem</label>
								</div>
								<div style="display: inline; margin-left: 5%;"
									class="form-input">
									<input type="checkbox" name="opcao" id="data" value="data"
										onclick="exibe_filtros(2);">
								</div>
								<div style="display: inline;" class="form-label">
									<label for="data">Data</label>
								</div>
							</div>
						</div>
	
	
	
	
						<div class="row text-center">
							<div class="col-sm-12 text-center">
								<div style="float: left; margin-left: 20px;"
									class="span3 text-left">
									<div id="CampoLocal" style="display: none;">
										<div class="form-label">
											<label id="escritaLocal" for="opcao_0">Local</label>
										</div>
										<div class="form-input">
											<input id="opc_0" name="opcao_0" type="text" />
										</div>
									</div>
								</div>
								<div style="float: left; margin-left: 20px;"
									class="span3 text-left">
									<div id="CampoOrigem" style="display: none;">
										<div class="form-label">
											<label for="opcao_1" id="escritaOrigem">Origem</label>
										</div>
										<div class="form-input">
											<input id="opc_1" name="opcao_1" type="text" />
										</div>
									</div>
								</div>
								<div style="float: left; margin-left: 20px;"
									class="span3 text-left">
									<div id="CampoDataI" style="display: none;">
										<div class="form-label">
											<label for="opcao_2" id="escritaDataI">Data Inicial</label>
										</div>
										<div class="form-input">
											<input id="opc_2" name="opcao_2" type="date" />
										</div>
									</div>
								</div>
								<div style="float: left; margin-left: 20px;"
									class="span3 text-left">
									<div id="CampoDataF" style="display: none;">
										<div class="form-label">
											<label for="opcao_3" id="escritaDataI">Data Final</label>
										</div>
										<div class="form-input">
											<input id="opc_3" name="opcao_3" type="date" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-sm-4 text-center">
							<a id="copyrights" href="Index.php">Página Inicial</a>
						</div>
					</div>
				</div>
				<br> <br>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="./_scripts/table-result.js"></script>

</body>
</html>

<?php


require 'APIs/Apache/Solr/Service.php';

// pega dados do link
$queries = @$_GET ['q'];
$local = @$_GET ['local'];
$origem = @$_GET ['origem'];
$inicio = @$_GET ['inicio'];
$fim = @$_GET ['fim'];

// chaves de conexão no Solr
define ( 'SOLR_SERVER_HOSTNAME', 'localhost' );
define ( 'SOLR_SERVER_USERNAME', 'admin' );
define ( 'SOLR_SERVER_PASSWORD', '' );
define ( 'SOLR_SERVER_PORT', 8983 );

// Conexão com o Solr
$client = new Apache_Solr_Service ( SOLR_SERVER_HOSTNAME, SOLR_SERVER_PORT, '/solr/corpus_twitter' );
$client->setAuthenticationCredentials ( SOLR_SERVER_USERNAME, SOLR_SERVER_PASSWORD );

$offset = 0;
$limit = 20;
$i=0;
$data0 = "$inicio.T00:00:00Z";
$date1 = "$fim.T00:00:00Z";
$textoAtual="fgds";
// protocolo de pesquisa
$response = $client->search ( $queries, $offset, $limit, $local, $origem, $data0, 'local', 'origem', 'data' );
if ($response->getHttpStatus () == 200) {
	
	if ($response->response->numFound > 0) {
		?>

<p id="logo">Resultados</p>
<br>
	<div class="container text-center">
		<div class="row text-center">
			<div class="col-sm-12 text-center">
				<table class="text-left" >
					<tbody>
										
						<tr>
							<td>Anterior</td>
							<td>Palavra</td>
							<td>Posterior</td>
							<td>info</td>
						</tr>		
<?php

						foreach ( $response->response->docs as $doc ) {
							$texto = $doc->texto;
							$tamanhoQuery = mb_strlen ( $queries ); // retorna o tamanho da string (tamanho da palavra pesquisada)
							$posicao = mb_strpos ( $texto, $queries ); // retorna a posicao da primeira ocorrencia da string
							if($posicao === false){
								continue;}
							$pre = mb_substr ( $texto, 0, $posicao ); // retorna string desde o inicio até a posicao da palavra pesquisada
							$now = mb_substr ( $texto, $posicao, $tamanhoQuery ); // retorna string da palavra pesquisada
							$pos = mb_substr ( $texto, $posicao + $tamanhoQuery ); // retorna string desde a posicao da palavra pesquisada até o fim
							
			
					?>
					
						<tr>
							<td><?php echo "$pre"; ?></td>
							<td><?php echo "$now"; ?></td>
							<td><?php echo "$pos";?></td>
								
								<td  style="border:yes; margin-right:1rem; width: 20rem;"><a><img id="img" width="30%" alt="ícone de informações" src="./_css/_img/icon-info.png"></a></td>
							</td>
						</tr>
					
			
						<?php
							while ( $posicao < mb_strrpos ( $texto, $queries ) ) { // realiza até que a posicao da proxima ocorencia da palavra pesquisada, seja a ultima
																				   
								// pega posicao da próxima ocorrencia em toda a frase pesquisada
								$posicao = mb_strlen ( $pre ) + $tamanhoQuery + mb_stripos ( $pos, $queries );
								
								$pre = mb_substr ( $texto, 0, $posicao ); // retorna string desde o inicio até a próxima posicao da palavra pesquisada
								$now = mb_substr ( $texto, $posicao, $tamanhoQuery ); // retorna string da palavra pesquisada
								$pos = mb_substr ( $texto, $posicao + $tamanhoQuery ); // retorna string da palavra pesquisada até o fim
						?>
						<tr>
							<td><?php echo "$pre"; ?></td>
							<td><?php echo "$now"; ?></td>
							<td><?php echo "$pos"; ?></td>
							<td style="border:yes; margin-right:1rem; width: 13%"><a onclick="visible(<?php echo "$i" ?>);"><img id="img" width="30%" alt="ícone de informações" src="./_css/_img/icon-info.png"></a></td>
											
						</tr>
					
					<?php
							}?>
							
							<?php
							
						}
					?>
					</tbody>
					</table>
					<?php
						} 
						
						else 
						
						{
							echo "<br>" + "Sorry, No Results";
						}
						
						}
						else 
						{
							// se não ter resposta, ele retorna a mensagem
							echo "$response->getHttpStatusMessage()";
						}
						
						include 'rodape.php';
						?>
