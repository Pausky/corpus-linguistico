<?php

// requer os arquivos de conexao ao DB e para enviar email
require_once ('./configurar.php');
require ("./APIs/phpmailer/class.phpmailer.php");
require ('./APIs/phpmailer/PHPMailerAutoload.php');

?>
<html>
<head>
<title>Capturas de Tela | Corpus Linguístico</title>
<?php // inicia a sessão
session_start ();
// se a sessão for falsa, volta pro login
if (isset ( $_SESSION ["Logado"] ) == false) {
	echo "<script>alert('Necessário Logar');</script>";
	header ( "Location: Login.php" );
}else{
	include("./menuLogado.php");
}?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="./_css/style-login-cadastro.css">
<link rel="stylesheet" type="text/css" href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css" href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css" href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">


</head>
<body>
	
	<br>
	<div class="container text-center">
		<div class="row text-center">
			<div class="col-sm-12 text-center">
				<h3 class="text-success text-center">Capturas de Tela</h3>
				<form action="?go=enviar" method="post" enctype=”multipart/form-data” style="background-color: #FFF5EE; width: 150%">
					<div class="container text-center">
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-input">					
									<p>Envie sua captura de tela do Corpus:</p>
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div style="display: inline-block;" class="form-input">
									<input type="hidden" name="MAX_FILE_SIZE" value="send">
									<input type="file" name="upload" id="upload" required/>
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 30%; border: 0px; padding: 0.3%" type="submit" class="btn-main" name="foto" value="enviar" />
								</div>
							</div>
						</div>
						<br>
					</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-sm-5 text-center">
							<a id="copyrights" href="Index.php">Página Inicial</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function cancelar(){
			location.href="Login.php";
		}
	</script><br><br><br>
</body>
<html>

<?php 

include 'rodape.php';

if (@$_GET ['go'] == "enviar") {
	
	if(isset($_FILES['upload'])){
		$username = $_SESSION['username'];
		$nome = $_FILES['upload']['name'];
		$tipo = $_FILES['upload']['type'];
		$tmp_nome = $_FILES['upload']['tmp_name'];
		$errors = $_FILES['upload']['error'];
		
		$tipos_img = array('image/pjpeg','image/jpeg','image/jpeg','image/JPG','image/X-PNG','image/PNG','image/png','image/x-png',);
		
		if(inArray($tipo, $tipos_img)){
			if(move_uploaded_file($tmp_nome, "./../uploads/{$nome}")){
				echo "<p><em>O arquivo já foi enviado</em></p>";
			}
			$conexao->exec("INSERT INTO imagens (username) VALUES ('', '".$username."', '".$nome."', '".$tipo."')");
		}else{
			echo '<p class="error">Insira uma imagem .JPG ou .PNG</p>';
		}
		
		if($errors > 0 ){
			echo '<p class="error">O arquivo não pode ser enviado por que: <strong>';
			
			switch ($errors){
				case 1:
					print 'O arquivo excedeu o upload_max_filesize configurado em php.ini';
					break;
				case 2:
					print 'O arquivo excedeu o MAX_FILE_SIZE configurado no formuláro html';
					break;
				case 3:
					print 'O arquivo foi apenas parcialmente enviado';
					break;
				case 4:
					print 'O arquivo não foi enviado';
					break;
				case 5:
					print 'O folder temporário ou temporariamente não foi avaliado';				
					break;
				case 6:
					print 'Inativo para escrever no disco';				
					break;
				case 7:
					print 'O envio do arquivo parou';				
					break;
				case 8:
					print 'O arquivo excedeu o tamanho máximo de envio';
					break;
				default:
					break;
			}
			print '</strong></p>';
		}
		
		if(file_exists($tmp_nome) && is_file($tmp_nome)){
			unlink($tmp_nome);	
		}
	}else{
		echo "Não foi encontrada um id 'upload'";
	}
}
?>