<html>
<head>
<title>Login | Corpus Linguistíco</title>
<?php
session_start();
if (isset ( $_SESSION ["Logado"] ) == false) {
	include("./menu.php");
}else{
	echo "<script>alert('Já Logado!');</script>";
	header ( "Location: ./index.php" );	
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="Homepage Projeto PT-br, corpus linguistíco, buscas, linguagem, regionalização, Projeto PT-br">
<meta name="x-subsite-id" content="5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css" href="./_css/bootstrap.min.css?m=1396490701">
<link rel="stylesheet" type="text/css" href="./_css/main.css?m=1502115234">
<link rel="stylesheet" type="text/css" href="./_css/flexslider.css?m=1390320474">
<link rel="shortcut icon" href="./_css/_img/icone-guia.png">

</head>
<body>
	
	<br>
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12 text-center">
				<h3>Login and Password</h3>
			
				<form class="form-role" style="background-color: #FFF5EE;"
					action="?go=login" method="post">
					<div class="container text-center">
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-label">
									<label for="username">Nome de Usuário</label>
								</div>
								<div class="form-input">
									<input type="text" name="username" id="username" size="30"
										value="" required />
								</div>
							</div>
						</div>
						<br>

						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-label">
									<label for="password">Senha</label>
								</div>
								<div class="form-input">
									<input type="password" name="password" id="password" size="30"
										value="" required />
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="rememberpass">
									<input type="checkbox" name="rememberusername"
										id="rememberusername" value="1" /> <label
										for="rememberusername">Lembrar identificação de usuário</label>
								</div>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="forgetpass">
									<a href="./Redefinir.php">Esqueci usuário ou senha</a>
								</div>
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 50%; border: 0px; padding: 1%" type="submit" class="btn-main" name="login" value="   Acessar   " />
								</div>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-sm-5 text-center">
								<br>
								<p>Ou</p>
								
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-input">
									<input style="width: 30%; border: 0px; padding: 0.5%" type="button" class="btn-main" onclick="cadastrar();" value="Cadastrar" />
								</div>
							</div>
						</div>
						<br><br>
					</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-sm-6 text-center">
							<a id="copyrights" href="Index.php">Página Inicial</a>
						</div>
					</div>
				</div>
				<br><br><br>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	function cadastrar(){
		location.href="Register.php";
	}
	</script>
	

</body>
</html>

<?php
include 'rodape.php';
// requer arquivo de configuracao (com o banco)
require_once 'configurar.php';

if (@$_GET ['go'] == "login") {
	// pega os dados digitados
	$username = $_POST ['username'];
	$password = $_POST ['password'];
	
	// se os campos nao estiverem vazios ele realiza pesquisa checando se o nome e senha estão corretos e se a conta está ativa
	$queryl = $conexao->query ( "SELECT * FROM usuarios WHERE username = '$username' AND password = '$password' AND ativo=1" )->fetch ();
	$query = $conexao->query ( "SELECT * FROM usuarios WHERE username = '$username' AND password = '$password'" )->fetch ();
	$que = $conexao->query ( "SELECT * FROM usuarios WHERE username = '$username'" )->fetch ();
	// se o usuario e senha estivem corretos, assim como a conta ativa, vc loga.
	if ($queryl >= 1 && $query >= 1) {

		$_SESSION ["Logado"] = true;
		$_SESSION ["nome"] = $queryl [1];
		$_SESSION ["sobrenome"] = $queryl [2];
		$_SESSION ["email"] = $queryl [3];
		$_SESSION ["username"] = $queryl [4];
		$_SESSION ["password"] = $queryl [5];
		
		echo "<script>alert('Login Sucessfull!')</script>";
		echo '<meta http-equiv="refresh" content="1;URL=index.php"/>';
		//header ( "?" .$_SESSION['username'] ."");
		
	}
	// se o usuario e senha estiverem corretos, mas a conta nao estiver ativa, emite mensagem
	if ($queryl <= 0 && $query >= 1) {
		echo "<script>alert('Conta Não Ativada')</script>";
	}
	// se o usuario e senha estiverem incorretos, mas a conta nao estiver ativa, emite mensagem
	if ($query <= 0 && $que >= 1) {
		echo "<script>alert('Usuário e Senha Não Coincidem')</script>";
	}
	// se o usuario e senha estiverem incorretos, e a conta nao estiver ativa, emite mensagem
	if ($que <= 0) {
		echo "<script>alert('Conta Inexistente')</script>";
	}
}

?>
