# -*- coding: utf-8 -*-
from Aelius import Toqueniza
t = "Na Europa e nos Estados Unidos, a área da Linguística"
t = t.decode("utf-8")
sents = Toqueniza.PUNKT.tokenize(t)
for sent in sents:
    for token in Toqueniza.TOK_PORT.tokenize(sent):
        print "%s" % token
